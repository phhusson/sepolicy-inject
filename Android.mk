LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE := sepolicy-inject
LOCAL_SRC_FILES := sepolicy-inject.c
LOCAL_STATIC_LIBRARIES := libsepol libc
LOCAL_FORCE_STATIC_EXECUTABLE := true
LOCAL_C_INCLUDES := external/libsepol/include

include $(BUILD_EXECUTABLE)
